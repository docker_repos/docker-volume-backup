FROM ubuntu:22.04

ARG DEBIAN_FRONTEND=noninteractive

ENV RCLONE_CONFIG=/data/rclone.conf
ENV BACKUPTIME_CRON="0 5 * * *"
ENV TZ=Europe/Amsterdam

COPY ./root/ /

RUN mkdir /data/
RUN mkdir /temp/

RUN touch /data/hostname
RUN echo "docker-volume-backup" > /data/hostname

RUN apt update
RUN apt install tar curl busybox cron tzdata -y

RUN curl https://rclone.org/install.sh | bash

VOLUME ["/volumes"]

ENTRYPOINT ["sh", "/entrypoint.sh"]
